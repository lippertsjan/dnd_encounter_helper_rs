use crate::encounter_model::Difficulty::{Deadly, Easy, Hard, Medium, Overkill};
use std::fmt;
use std::fmt::Formatter;

#[derive(Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Hash, Debug)]
pub enum Difficulty {
    Easy,
    Medium,
    Hard,
    Deadly,
    Overkill,
}

impl fmt::Display for Difficulty {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{:?}", self)
    }
}

// todo better name
#[derive(Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Hash, Debug)]
pub struct EncounterComputation<'a> {
    pub difficulty: Difficulty,
    pub player_group: &'a PlayerGroup,
    pub encounter: &'a Encounter,
}

impl fmt::Display for EncounterComputation<'_> {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{:-<8} {} {}",
            self.difficulty,
            self.encounter.adjusted_xp(),
            self.player_group.xp_thresholds()
        )
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Hash, Debug, Default)]
pub struct PlayerGroup {
    pub size: i32,
    pub level: i32,
}

impl PlayerGroup {
    pub fn xp_thresholds(&self) -> Thresholds {
        match self.get_xp_threshold_of_pc() {
            Thresholds {
                easy,
                medium,
                hard,
                deadly,
            } => Thresholds {
                easy: self.size * easy,
                medium: self.size * medium,
                hard: self.size * hard,
                deadly: self.size * deadly,
            },
        }
    }

    fn get_xp_threshold_of_pc(&self) -> Thresholds {
        Self::xp_threshold_table()[self.level as usize]
    }

    fn xp_threshold_table() -> Vec<Thresholds> {
        vec![
            Thresholds {
                easy: 0,
                medium: 0,
                hard: 0,
                deadly: 0,
            },
            Thresholds {
                easy: 25,
                medium: 50,
                hard: 75,
                deadly: 100,
            },
            Thresholds {
                easy: 50,
                medium: 100,
                hard: 150,
                deadly: 200,
            },
            Thresholds {
                easy: 75,
                medium: 150,
                hard: 225,
                deadly: 400,
            },
            Thresholds {
                easy: 125,
                medium: 250,
                hard: 375,
                deadly: 500,
            },
            Thresholds {
                easy: 250,
                medium: 500,
                hard: 750,
                deadly: 1100,
            },
            Thresholds {
                easy: 300,
                medium: 600,
                hard: 900,
                deadly: 1400,
            },
            Thresholds {
                easy: 350,
                medium: 750,
                hard: 1100,
                deadly: 1700,
            },
            Thresholds {
                easy: 450,
                medium: 900,
                hard: 1400,
                deadly: 2100,
            },
            Thresholds {
                easy: 550,
                medium: 1100,
                hard: 1600,
                deadly: 2400,
            },
            Thresholds {
                easy: 600,
                medium: 1200,
                hard: 1900,
                deadly: 2800,
            },
            Thresholds {
                easy: 800,
                medium: 1600,
                hard: 2400,
                deadly: 3600,
            },
            Thresholds {
                easy: 1000,
                medium: 2000,
                hard: 3000,
                deadly: 4500,
            },
            Thresholds {
                easy: 1100,
                medium: 2200,
                hard: 3400,
                deadly: 5100,
            },
            Thresholds {
                easy: 1250,
                medium: 2500,
                hard: 3800,
                deadly: 5700,
            },
            Thresholds {
                easy: 1400,
                medium: 2800,
                hard: 4300,
                deadly: 6400,
            },
            Thresholds {
                easy: 1600,
                medium: 3200,
                hard: 4800,
                deadly: 7200,
            },
            Thresholds {
                easy: 2000,
                medium: 3900,
                hard: 5900,
                deadly: 8800,
            },
            Thresholds {
                easy: 2100,
                medium: 4200,
                hard: 6300,
                deadly: 9500,
            },
            Thresholds {
                easy: 2400,
                medium: 4900,
                hard: 7300,
                deadly: 10900,
            },
            Thresholds {
                easy: 2800,
                medium: 5700,
                hard: 8500,
                deadly: 12700,
            },
        ]
    }
}

impl fmt::Display for PlayerGroup {
    fn fmt(&self, f: &mut fmt::Formatter) -> fmt::Result {
        write!(f, "{}x Lvl {})", self.size, self.level)
    }
}

#[derive(Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Hash, Debug, Default)]
pub struct Thresholds {
    pub easy: i32,
    pub medium: i32,
    pub hard: i32,
    pub deadly: i32,
}

impl fmt::Display for Thresholds {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "({}, {}, {}, {})",
            self.easy, self.medium, self.hard, self.deadly
        )
    }
}

// todo use struct Monster from monsters.rs
#[derive(Clone, Copy, PartialEq, Eq, Ord, PartialOrd, Hash, Debug, Default)]
pub struct MonsterGroup {
    pub count: i32,
    pub xp: i32,
}

impl fmt::Display for MonsterGroup {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(f, "{}x {}XP", self.count, self.xp)
    }
}

#[derive(Clone, PartialEq, Eq, Ord, PartialOrd, Hash, Debug, Default)]
pub struct Encounter {
    pub monster_groups: Vec<MonsterGroup>,
}

impl fmt::Display for Encounter {
    fn fmt(&self, f: &mut Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{{{}}}",
            &self
                .monster_groups
                .iter()
                .map(|m| format!("{}", m))
                .collect::<Vec<_>>()
                .join(", ")
        )
    }
}

impl Encounter {
    pub fn count(&self) -> i32 {
        self.sum_on_mapped_vec(|g| g.count)
    }

    pub fn total_xp(&self) -> i32 {
        self.sum_on_mapped_vec(|g| g.count * g.xp)
    }

    fn sum_on_mapped_vec(&self, f: fn(&MonsterGroup) -> i32) -> i32 {
        self.monster_groups.iter().map(f).sum()
    }

    pub fn adjusted_xp(&self) -> f32 {
        let total_xp = self.total_xp() as f32;
        self.multiplier() * total_xp
    }

    fn multiplier(&self) -> f32 {
        let monster_count = self.count();

        if monster_count == 1 {
            1.0
        } else if monster_count == 2 {
            1.5
        } else if 3 <= monster_count && monster_count <= 6 {
            2.0
        } else if 7 <= monster_count && monster_count <= 10 {
            2.5
        } else if 11 <= monster_count && monster_count <= 14 {
            3.0
        } else {
            4.0
        }
    }

    pub(crate) fn new() -> Self {
        Self {
            monster_groups: vec![],
        }
    }
}

pub fn determine_difficulty<'a>(
    player_group: &'a PlayerGroup,
    encounter: &'a Encounter,
) -> EncounterComputation<'a> {
    let thresholds = player_group.xp_thresholds();
    let adjusted_xp = encounter.adjusted_xp();
    let difficulty = calculate_difficulty_class(thresholds, adjusted_xp);

    EncounterComputation {
        difficulty,
        player_group,
        encounter,
    }
}

fn calculate_difficulty_class(thresholds: Thresholds, adjusted_xp: f32) -> Difficulty {
    if adjusted_xp <= thresholds.easy as f32 {
        Easy
    } else if adjusted_xp <= thresholds.medium as f32 {
        Medium
    } else if adjusted_xp <= thresholds.hard as f32 {
        Hard
    } else if adjusted_xp <= thresholds.deadly as f32 {
        Deadly
    } else {
        Overkill
    }
}

#[cfg(test)]
mod tests {
    mod encounter {
        use crate::encounter_model::Encounter;

        use super::super::*;

        #[test]
        fn encounter_total_count() {
            assert_eq!(
                1,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 1, xp: 50 })
                }
                .count()
            );

            assert_eq!(
                2,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 2, xp: 50 },)
                }
                .count()
            );
            assert_eq!(
                2,
                Encounter {
                    monster_groups: vec!(
                        MonsterGroup { count: 1, xp: 50 },
                        MonsterGroup { count: 1, xp: 50 },
                    )
                }
                .count()
            );

            assert_eq!(
                3,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 3, xp: 50 },)
                }
                .count()
            );
            assert_eq!(
                3,
                Encounter {
                    monster_groups: vec!(
                        MonsterGroup { count: 2, xp: 50 },
                        MonsterGroup { count: 1, xp: 50 },
                    )
                }
                .count()
            );
            assert_eq!(
                3,
                Encounter {
                    monster_groups: vec!(
                        MonsterGroup { count: 1, xp: 50 },
                        MonsterGroup { count: 2, xp: 50 },
                    )
                }
                .count()
            );
            assert_eq!(
                3,
                Encounter {
                    monster_groups: vec!(
                        MonsterGroup { count: 1, xp: 50 },
                        MonsterGroup { count: 1, xp: 50 },
                        MonsterGroup { count: 1, xp: 50 },
                    )
                }
                .count()
            );
        }

        #[test]
        fn encounter_xp_multiplier_1() {
            assert_eq!(
                1f32,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 1, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                1f32,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 1, xp: 50 })
                }
                .multiplier()
            );
        }

        #[test]
        fn encounter_xp_multiplier_1_5() {
            assert_eq!(
                1.5f32,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 2, xp: 50 })
                }
                .multiplier()
            );
        }

        #[test]
        fn encounter_xp_multiplier_2() {
            let expected_multiplier = 2f32;
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 3, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 4, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 5, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 6, xp: 50 })
                }
                .multiplier()
            );
        }

        #[test]
        fn encounter_xp_multiplier_2_5() {
            let expected_multiplier = 2.5f32;
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 7, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 8, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 9, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 10, xp: 50 })
                }
                .multiplier()
            );
        }

        #[test]
        fn encounter_xp_multiplier_3_5() {
            let expected_multiplier = 3f32;
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 11, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 12, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 13, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 14, xp: 50 })
                }
                .multiplier()
            );
        }

        #[test]
        fn encounter_xp_multiplier_4() {
            let expected_multiplier = 4f32;
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 15, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 16, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 17, xp: 50 })
                }
                .multiplier()
            );
            assert_eq!(
                expected_multiplier,
                Encounter {
                    monster_groups: vec!(MonsterGroup { count: 18, xp: 50 })
                }
                .multiplier()
            );
        }

        #[test]
        fn _1_100() {
            let encounter = Encounter {
                monster_groups: vec![MonsterGroup { count: 1, xp: 100 }],
            };
            assert_eq!(100, encounter.total_xp());
            assert_eq!(1f32, encounter.multiplier());
            assert_eq!(100f32, encounter.adjusted_xp());
        }

        #[test]
        fn _2_100() {
            let encounter = Encounter {
                monster_groups: vec![MonsterGroup { count: 2, xp: 100 }],
            };
            assert_eq!(200, encounter.total_xp());
            assert_eq!(1.5f32, encounter.multiplier());
            assert_eq!(300f32, encounter.adjusted_xp());
        }

        #[test]
        fn encounter_adjusted_xp() {
            assert_eq!(
                Encounter {
                    monster_groups: vec![MonsterGroup { count: 1, xp: 100 }]
                }
                .adjusted_xp(),
                100f32
            );

            assert_eq!(
                Encounter {
                    monster_groups: vec![MonsterGroup { count: 2, xp: 100 }]
                }
                .adjusted_xp(),
                1.5 * 200f32
            );
            //
            // assert_eq!(
            //     Encounter { monster_groups: vec![MonsterGroup{count: 1, xp: 100}]}.adjusted_xp(),
            //     100f32);
            //
            // assert_eq!(
            //     Encounter { monster_groups: vec![MonsterGroup{count: 1, xp: 100}]}.adjusted_xp(),
            //     100f32);
            //
            // assert_eq!(
            //     Encounter { monster_groups: vec![MonsterGroup{count: 1, xp: 100}]}.adjusted_xp(),
            //     100f32);
            //
            // assert_eq!(
            //     Encounter { monster_groups: vec![MonsterGroup{count: 1, xp: 100}]}.adjusted_xp(),
            //     100f32);
            //
            // assert_eq!(
            //     Encounter { monster_groups: vec![MonsterGroup{count: 1, xp: 100}]}.adjusted_xp(),
            //     100f32)
        }
    }

    mod player_group {
        use super::super::*;

        #[test]
        fn thresholds_level_1() {
            assert_eq!(
                Thresholds {
                    easy: 25,
                    medium: 50,
                    hard: 75,
                    deadly: 100
                },
                PlayerGroup { size: 1, level: 1 }.xp_thresholds()
            );
            assert_eq!(
                Thresholds {
                    easy: 50,
                    medium: 100,
                    hard: 150,
                    deadly: 200
                },
                PlayerGroup { size: 2, level: 1 }.xp_thresholds()
            );
            assert_eq!(
                Thresholds {
                    easy: 75,
                    medium: 150,
                    hard: 225,
                    deadly: 300
                },
                PlayerGroup { size: 3, level: 1 }.xp_thresholds()
            );
        }

        #[test]
        fn thresholds_level_2() {
            for size in 0..20 {
                assert_eq!(
                    Thresholds {
                        easy: size * 25,
                        medium: size * 50,
                        hard: size * 75,
                        deadly: size * 100
                    },
                    PlayerGroup { size, level: 1 }.xp_thresholds()
                );
            }
        }

        #[test]
        fn thresholds_level_3() {
            for size in 0..20 {
                assert_eq!(
                    Thresholds {
                        easy: size * 75,
                        medium: size * 150,
                        hard: size * 225,
                        deadly: size * 400
                    },
                    PlayerGroup { size, level: 3 }.xp_thresholds()
                );
            }
        }

        #[test]
        fn thresholds_level_5() {
            for size in 0..20 {
                assert_eq!(
                    Thresholds {
                        easy: size * 250,
                        medium: size * 500,
                        hard: size * 750,
                        deadly: size * 1100
                    },
                    PlayerGroup { size, level: 5 }.xp_thresholds()
                );
            }
        }

        #[test]
        fn thresholds_level_10() {
            for size in 0..20 {
                assert_eq!(
                    Thresholds {
                        easy: size * 600,
                        medium: size * 1200,
                        hard: size * 1900,
                        deadly: size * 2800
                    },
                    PlayerGroup { size, level: 10 }.xp_thresholds()
                );
            }
        }

        #[test]
        fn thresholds_level_15() {
            for size in 0..20 {
                assert_eq!(
                    Thresholds {
                        easy: size * 1400,
                        medium: size * 2800,
                        hard: size * 4300,
                        deadly: size * 6400
                    },
                    PlayerGroup { size, level: 15 }.xp_thresholds()
                );
            }
        }

        #[test]
        fn thresholds_level_20() {
            for size in 0..20 {
                assert_eq!(
                    Thresholds {
                        easy: size * 2800,
                        medium: size * 5700,
                        hard: size * 8500,
                        deadly: size * 12700
                    },
                    PlayerGroup { size, level: 20 }.xp_thresholds()
                );
            }
        }
    }

    mod calculate_difficulty_class {
        use super::super::*;

        #[test]
        fn easy() {
            assert_eq!(
                Easy,
                calculate_difficulty_class(
                    Thresholds {
                        easy: 100,
                        medium: 200,
                        hard: 300,
                        deadly: 400
                    },
                    0f32
                )
            )
        }

        #[test]
        fn medium() {
            assert_eq!(
                Medium,
                calculate_difficulty_class(
                    Thresholds {
                        easy: 100,
                        medium: 200,
                        hard: 300,
                        deadly: 400
                    },
                    150f32
                )
            )
        }

        #[test]
        fn hard() {
            assert_eq!(
                Hard,
                calculate_difficulty_class(
                    Thresholds {
                        easy: 100,
                        medium: 200,
                        hard: 300,
                        deadly: 400
                    },
                    250f32
                )
            )
        }

        #[test]
        fn deadly() {
            assert_eq!(
                Deadly,
                calculate_difficulty_class(
                    Thresholds {
                        easy: 100,
                        medium: 200,
                        hard: 300,
                        deadly: 400
                    },
                    350f32
                )
            )
        }

        #[test]
        fn overkill() {
            assert_eq!(
                Overkill,
                calculate_difficulty_class(
                    Thresholds {
                        easy: 100,
                        medium: 200,
                        hard: 300,
                        deadly: 400
                    },
                    450f32
                )
            )
        }
    }

    use super::*;
}
