mod encounter_model;
mod monsters;

use crate::encounter_model::{
    determine_difficulty, Encounter, MonsterGroup, PlayerGroup, Thresholds,
};
use crate::monsters::do_something;
use std::convert::TryInto;
use std::io::{stdin, stdout, Write};

pub fn monster_csv() -> Result<(), String> {
    do_something()
}

pub fn calculate_encounter_dcs() -> Result<(), String> {
    loop {
        let player_group = read_player_group()?;
        let xp_threshold_for_group = player_group.xp_thresholds();
        println!("{:?}", &xp_threshold_for_group);

        let mut encounter = read_encounter()?;
        while encounter.count() > 0 {
            let adjusted_xp = encounter.adjusted_xp();

            let encounter_computation = determine_difficulty(&player_group, &encounter);
            println!("{}", encounter_computation);

            encounter = read_encounter()?;
        }
    }
}

fn print_difficulty(xp_threshold_for_group: Thresholds, adjusted_xp: f32) {
    match xp_threshold_for_group {
        Thresholds {
            easy,
            medium,
            hard,
            deadly,
        } => {
            let easy = easy as f32;
            let medium = medium as f32;
            let hard = hard as f32;
            let deadly = deadly as f32;

            if adjusted_xp <= easy {
                print!("Easy")
            } else if adjusted_xp <= medium {
                print!("Medium")
            } else if adjusted_xp <= hard {
                print!("Hard")
            } else if adjusted_xp <= deadly {
                print!("Deadly")
            } else {
                print!("OVERKILL")
            }
        }
    }

    println!(
        ". Adjusted XP: {:?}, Thresholds: {:?}",
        adjusted_xp, xp_threshold_for_group
    );
}

fn read_encounter() -> Result<Encounter, String> {
    let format_msg = "(<# mon> <exp>)+: ";

    let split_s = read_line_as_vec(format_msg);
    let integers = split_s
        .into_iter()
        .map(|x| x.parse::<i32>().unwrap())
        .collect::<Vec<_>>();

    let len = integers.len();
    let mmod = len % 2;

    if len == 0 {
        return Ok(Encounter::new());
    }

    if mmod != 0 {
        let error_msg = format!("Input is not in the correct format: \"{}\".", format_msg);
        return Err(String::from(error_msg));
    }
    let lower_half: i32 = (integers.len() / 2).try_into().unwrap();

    Ok(Encounter {
        monster_groups: {
            let mut monster_groups: Vec<MonsterGroup> = vec![];
            for i in 0..lower_half {
                let i1 = 2 * i as usize;
                let i2 = (2 * i + 1) as usize;

                monster_groups.push(MonsterGroup {
                    count: integers[i1],
                    xp: integers[i2],
                });
            }

            monster_groups
        },
    })
}

fn read_player_group() -> Result<PlayerGroup, String> {
    let format_msg = "<# players> <level>: ";

    let split_s = read_line_as_vec(format_msg);

    if split_s.len() != 2 {
        let error_msg = format!("Input is not in correct format: \"{}\".", format_msg);
        return Err(String::from(error_msg));
    } else {
        let integers = split_s
            .into_iter()
            .map(|x| x.parse::<i32>().unwrap())
            .collect::<Vec<_>>();

        let size = integers[0];
        let level = integers[1];

        return Ok(PlayerGroup { size, level });
    }
}

fn read_line_as_vec(format_msg: &str) -> Vec<String> {
    let s = read_line(format_msg);

    let split_s = s
        .split(" ")
        .map(String::from)
        .filter(|s| !s.is_empty())
        .collect::<Vec<_>>();

    split_s
}

fn read_line(msg: &str) -> String {
    print!("{}", msg);

    let mut s = String::new();

    let _ = stdout().flush();
    stdin()
        .read_line(&mut s)
        .expect("Did not enter a correct string");

    if let Some('\n') = s.chars().next_back() {
        s.pop();
    }
    if let Some('\r') = s.chars().next_back() {
        s.pop();
    }

    return s;
}
