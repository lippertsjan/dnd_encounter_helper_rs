use std::fs::File;
use std::io::{self, BufRead};
use std::path::Path;

use std::error::Error;

pub fn do_something() -> Result<(), String> {
    if let Ok(lines) =
        read_lines("../../Downloads/Monster Spreadsheet (D&D5e) - Official Stats.csv")
    {
        let monsters = lines.skip(1).map(|line_result| {
            match line_result {
                Ok(line) =>  {
                let raw_data_line = parse_raw_data_line(line);
                convert_raw_data_line(raw_data_line)
            },
            Err(e) => Err(Box::from(e)),
            }
        }).collect::<Vec<_>>();
        

        //for m in monsters {
        //    println!("{:?}", m);
        //}
        println!("{}", monsters.len());
    }

    Ok(())
}

fn parse_raw_data_line(line: String) -> Vec<String> {
    let mut raw_data_line_entries: Vec<String> = vec![];

    let mut is_currently_string_entry = false;
    let mut current_entry = String::new();

    for potential_entry in line.split(",") {
        let is_string_entry_start = potential_entry.starts_with("\"");
        let is_string_entry_end = is_currently_string_entry && potential_entry.ends_with("\"");

        if is_currently_string_entry {
            current_entry.push_str(", ");
        }
        current_entry.push_str(potential_entry);

        if is_string_entry_start {
            is_currently_string_entry = true;
        } else if is_currently_string_entry && is_string_entry_end {
            is_currently_string_entry = false;
            raw_data_line_entries.push(current_entry.clone());
            current_entry.clear();
        } else if is_currently_string_entry {
            // do nothing. everything is already done
        } else {
            // single non-string entry
            raw_data_line_entries.push(current_entry.clone());
            current_entry.clear();
        }
    }

    raw_data_line_entries
}

fn convert_raw_data_line(data_line: Vec<String>) -> Result<Monster, Box<dyn Error>> {
    let mut iter = data_line.into_iter();

    let name = iter.next().ok_or_else(|| "invalid name entry")?;
    let size = iter.next().ok_or_else(|| "invalid size entry")?;
    let creature_type = iter.next().ok_or_else(|| "invalid type entry")?;
    let alignment = iter.next().ok_or_else(|| "invalid alignment entry")?;
    let ac_s = iter.next().ok_or_else(|| "invalid AC entry")?;
    let hp_s = iter.next().ok_or_else(|| "invalid HP entry")?;
    let speeds = iter.next().ok_or_else(|| "invalid speeds entry")?;
    let str_s = iter.next().ok_or_else(|| "invalid STR entry")?;
    let dex_s = iter.next().ok_or_else(|| "invalid DEX entry")?;
    let con_s = iter.next().ok_or_else(|| "invalid CON entry")?;
    let int_s = iter.next().ok_or_else(|| "invalid INT entry")?;
    let wis_s = iter.next().ok_or_else(|| "invalid WIS entry")?;
    let cha_s = iter.next().ok_or_else(|| "invalid CHA entry")?;
    let saving_throws = iter.next().ok_or_else(|| "invalid saving throws entry")?;
    let skills = iter.next().ok_or_else(|| "invalid skills entry")?;
    let wri = iter.next().ok_or_else(|| "invalid wri entry")?;
    let senses = iter.next().ok_or_else(|| "invalid senses entry")?;
    let languages = iter.next().ok_or_else(|| "invalid languages entry")?;
    let cr_s = iter.next().ok_or_else(|| "invalid CR entry")?;
    let additional = iter.next().ok_or_else(|| "invalid additional entry")?;
    let font = iter.next().ok_or_else(|| "invalid font entry")?;
    let additional_info = iter.next().ok_or_else(|| "invalid additional info entry")?;
    let author = iter.next().ok_or_else(|| "invalid author entry")?;
    
    let ac = ac_s.parse::<i32>().or_else(|_| Err("Invalid AC"))?;
    let hp = hp_s.parse::<i32>().or_else(|_| Err("Invalid HP"))?;
    let str_i = str_s.parse::<i32>().or_else(|_| Err("Invalid STR"))?;
    let dex = dex_s.parse::<i32>().or_else(|_| Err("Invalid DEX"))?;
    let con = con_s.parse::<i32>().or_else(|_| Err("Invalid CON"))?;
    let int = int_s.parse::<i32>().or_else(|_| Err("Invalid INT"))?;
    let wis = wis_s.parse::<i32>().or_else(|_| Err("Invalid WIS"))?;
    let cha = cha_s.parse::<i32>().or_else(|_| Err("Invalid CHA"))?;
    let cr  = cr_s.parse::<f32>().or_else(|_| Err("Invalid CR"))?;

    // Name,Size,Type,Align.,AC,HP,Speeds,STR,DEX,CON,INT,WIS,CHA,Sav. Throws,Skills,WRI,Senses,Languages,CR,Additional,Font,Additional Info,Author


    Ok(Monster{
        name,
        size,
        creature_type,
        alignment,
        ac,
        hp,
        speeds,
        str: str_i,
        dex,
        con,
        int,
        wis,
        cha,
        saving_throws,
        skills,
        wri,
        senses,
        languages,
        cr,
        additional,
        font,
        additional_info,
        author
    })
}

// The output is wrapped in a Result to allow matching on errors
// Returns an Iterator to the Reader of the lines of the file.
fn read_lines<P>(filename: P) -> io::Result<io::Lines<io::BufReader<File>>>
where
    P: AsRef<Path>,
{
    let file = File::open(filename)?;
    Ok(io::BufReader::new(file).lines())
}

#[derive(Clone, PartialEq, PartialOrd, Debug, Default)]
pub struct Monster {
    name: String,
    size: String,
    creature_type: String,
    alignment: String,
    ac: i32,
    hp: i32,
    speeds: String,
    str: i32,
    dex: i32,
    con: i32,
    int: i32,
    wis: i32,
    cha: i32,
    saving_throws: String,
    skills: String,
    wri: String,
    senses: String,
    languages: String,
    cr: f32,
    additional: String,
    font: String,
    additional_info: String,
    author: String,
}

impl Monster {
    // todo use actual xp in data
    pub fn xp_from_cr(&self) -> i32 {
        let scaled_cr = (8f32 * self.cr) as i32;
        match scaled_cr {
             0  =>     10,
             1 =>     25,
             4   =>     50,
             8  =>    100,
             16  =>    200,
             24  =>    450,
             32  =>    700,
             40  =>   1100,
             48  =>   1800,
             56  =>   2300,
             64  =>   2900,
             72  =>   3900,
            80  =>   5000,
            88  =>   7200,
            96  =>   8400,
            104  =>  10000,
            112  =>  11500,
            120  =>  13000,
            128  =>  15000,
            136  =>  18000,
            144  =>  20000,
            152  =>  22000,
            160  =>  25000,
            168  =>  33000,
            176  =>  41000,
            184  =>  50000,
            192  =>  62000,
            200  =>  75000,
            208  =>  90000,
            216  => 105000,
            224  => 120000,
            232  => 135000,
            240  => 155000,
            _ => -1,
        }
    }
}
